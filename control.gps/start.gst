<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="start" role="graph" edgeids="false" edgemode="directed">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n215"/>
        <node id="n216"/>
        <node id="n217"/>
        <node id="n218"/>
        <node id="n214"/>
        <node id="n211"/>
        <node id="n212"/>
        <node id="n219"/>
        <node id="n213"/>
        <edge from="n214" to="n219">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n217" to="n216">
            <attr name="label">
                <string>now</string>
            </attr>
        </edge>
        <edge from="n214" to="n213">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n215" to="n214">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n213" to="n219">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n216" to="n214">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n213" to="n218">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n212" to="n211">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n219" to="n212">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n216" to="n215">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
    </graph>
</gxl>

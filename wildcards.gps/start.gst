<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n0"/>
        <node id="n1"/>
        <node id="n2"/>
        <edge from="n0" to="n2">
            <attr name="label">
                <string>b</string>
            </attr>
        </edge>
        <edge from="n1" to="n0">
            <attr name="label">
                <string>a</string>
            </attr>
        </edge>
        <edge from="n2" to="n1">
            <attr name="label">
                <string>c</string>
            </attr>
        </edge>
    </graph>
</gxl>

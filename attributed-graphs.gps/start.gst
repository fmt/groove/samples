<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n0"/>
        <node id="n1"/>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>int:0</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>Buffer</string>
            </attr>
        </edge>
        <edge from="n1" to="n0">
            <attr name="label">
                <string>length</string>
            </attr>
        </edge>
    </graph>
</gxl>

<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph id="graph" role="graph" edgeids="false" edgemode="directed">
        <node id="n22"/>
        <node id="n18"/>
        <node id="n20"/>
        <node id="n19"/>
        <node id="n21"/>
        <node id="n17"/>
        <node id="n23"/>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n18" to="n23">
            <attr name="label">
                <string>3</string>
            </attr>
        </edge>
        <edge from="n18" to="n23">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n20" to="n23">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n20" to="n23">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n21" to="n23">
            <attr name="label">
                <string>2</string>
            </attr>
        </edge>
        <edge from="n21" to="n23">
            <attr name="label">
                <string>1</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>A</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>B</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>C</string>
            </attr>
        </edge>
    </graph>
</gxl>
